@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Task details
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('task.update', $task->id) }}">
        @method('PATCH')
        @csrf
      <div class="form-group">
        @csrf
        <label for="name">Title:</label>
        <input type="text" class="form-control" name="title" value="{{$task->time}}" />
      </div>
      <div class="form-group">
        <label for="price">task:</label>
        <textarea class="form-control" name="task">{{$task->task}}</textarea>
      </div>
      <div class="form-group">
        <label for="input_starttime">Time</label> 
        <input placeholder="Selected time" name="time" type="text" id="input_starttime" class="form-control timepicker" value="{{$task->time}}" >
      </div>
      <div class="form-group">
        <label for="input_date">Date</label> 
        <input placeholder="Selected date" name="date" type="date" id="input_date" class="form-control datepicker" value="{{$task->date}}" >
      </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection