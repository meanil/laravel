<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Laravel Example Tutorial To Do Task</title>
	<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/timepicki.css') }}">

</head>
<body>
	<div class="container">
		@yield('content')
	</div>
	<script src="{{ asset('js/app.js') }}" type="text/js"></script>
	<!-- <script src="{{ asset('js/jquery.js') }}"></script> -->
	<script
	src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
	<script src="{{ asset('js/timepicki.js') }}"></script>
	<!-- <script src="{{ asset('js/custom.js') }}" type="text/js"></script> -->
	<script>
		$(document).ready(function(){
			$(".timepicker").timepicki();
		});
	</script>
</body>
</html>