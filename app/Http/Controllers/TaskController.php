<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tasks = Task::all();//assign list all list to tasks

        return view('task.index', compact('tasks'));//defines view and passing tasks
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  
       return view('task.create');

   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      $request->validate([
        'title'=>'required|string',
        'task'=> 'required|string',
        'time' => 'required|string',
        'date' => 'required|string'
    ]); //validate the form value

      $task = new Task([
        'title' => $request->get('title'),
        'task'=> $request->get('task'),
        'time'=> $request->get('time'),
        'date'=> $request->get('date'),
    ]);//assign form values to object
      $task->save(); //saving object to database
      return redirect('/task')->with('success', 'task has been added');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $task = Task::find($id);

        return view('task.details', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $task = Task::find($id);

        return view('task.details', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //
              $request->validate([
        'title'=>'required|string',
        'task'=> 'required|string',
        'time' => 'required|string',
        'date' => 'required|string'
      ]);

      $task = Task::find($id);
      $task->title = $request->get('title');
      $task->task = $request->get('task');
      $task->time = $request->get('time');
      $task->date = $request->get('date');
      $task->save();

      return redirect('/task')->with('success', 'Task has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
             $task = Tk::find($id);
     $task->delete();

     return redirect('/task')->with('success', 'Task has been deleted Successfully');
    }
}
